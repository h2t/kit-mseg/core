/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::PenalisedErrorMetric
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/components/EvaluationController/PenalisedErrorMetric.h>

double mseg::PenalisedErrorMetric::maxDistance = 1.0f;
double mseg::PenalisedErrorMetric::missedKeyframesPenalty = 7.0f;
double mseg::PenalisedErrorMetric::falsePositiveKeyframesPenalty = 7.0f;

mseg::EvaluationResultPtr
mseg::PenalisedErrorMetric::evaluate(std::vector<int> groundTruth, std::vector<int> test)
{
    mseg::EvaluationResultPtr result = new EvaluationResult();
    result->name = "Penalised squared error approach";
    result->isGroup = false;
    result->groupName = "approach";
    result->groupAttributeName = "name";
    result->groupAttributeValue = result->name;

    double error = 0.0f;

    int unmatchedKeyframesNumber = test.size();

    for (int gt : groundTruth)
    {
        double bestDistance = FLT_MAX;
        int i = 0;
        int bestKeyframe = - 1;

        for (int t : test)
        {
            int distance = std::abs(t - gt);

            if (distance < bestDistance)
            {
                bestDistance = distance;
                bestKeyframe = i;
            }

            i += 1;
        }

        if (bestDistance < mseg::PenalisedErrorMetric::maxDistance)
        {
            error += bestDistance * bestDistance;
            unmatchedKeyframesNumber -= 1;

            std::vector<int>::iterator it = std::find(test.begin(), test.end(), bestKeyframe);

            if (it != test.end())
            {
                test.erase(it);
            }
        }
        else
        {
            error += mseg::PenalisedErrorMetric::missedKeyframesPenalty;
        }
    }

    error += mseg::PenalisedErrorMetric::falsePositiveKeyframesPenalty * unmatchedKeyframesNumber;

    // Set score
    result->score = error;
    result->isScoreInt = true;

    return result;
}
