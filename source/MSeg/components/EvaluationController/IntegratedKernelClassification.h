/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::IntegratedKernelClassification
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_MSeg_IntegratedKernelClassification_H
#define _ARMARX_COMPONENT_MSeg_IntegratedKernelClassification_H

#include <cmath>
#include <math.h>
#include <tuple>
#include <vector>

#include <ArmarXCore/core/logging/Logging.h>

#include <MSeg/interface/DataTypes.h>

namespace mseg
{
    class IntegratedKernelClassification
    {

    private:

        static double sigma;

        IntegratedKernelClassification();

    public:

        static std::tuple<mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr>
        getClassification(std::vector<int>, std::vector<int>, int);

    private:

        static double gaussian(double, double, double);
        static double gaussianSum(double, std::vector<int>, int);
        static std::tuple<std::vector<double>, std::vector<double> > splitClassificationError(std::vector<double>);
        static double integrateTrapz(double intStep, std::vector<double>);

    };
}

#endif
