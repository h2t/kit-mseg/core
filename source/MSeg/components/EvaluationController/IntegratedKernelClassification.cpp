/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::IntegratedKernelClassification
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/components/EvaluationController/IntegratedKernelClassification.h>

double mseg::IntegratedKernelClassification::sigma = 6.667f;

std::tuple<mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr>
mseg::IntegratedKernelClassification::getClassification(std::vector<int> groundTruth, std::vector<int> test, int frameCount)
{
    // Integration borders
    double intMin = 0.0f - (3 * mseg::IntegratedKernelClassification::sigma);
    double intMax = (double) frameCount + (3 * mseg::IntegratedKernelClassification::sigma);
    double intStep = 0.05f;

    // Error functions (y-values)
    std::vector<double> c_s;
    std::vector<double> e_c;
    std::vector<double> e_fp;
    std::vector<double> e_fn;

    // Calculate y values for error functions
    for (double x = intMin; x < intMax; x += intStep)
    {
        double c_s_value = mseg::IntegratedKernelClassification::gaussianSum(x, test, 1);
        double c_gt_value = mseg::IntegratedKernelClassification::gaussianSum(x, groundTruth, -1);

        c_s.push_back(c_s_value);
        e_c.push_back(c_s_value + c_gt_value);
    }

    // Split e_c into e_fp and e_fn
    std::tie(e_fp, e_fn) = mseg::IntegratedKernelClassification::splitClassificationError(e_c);

    // Calculate integrals for e_fp and e_fn to get fp and fn. Calculate tp and tn then
    double fp = mseg::IntegratedKernelClassification::integrateTrapz(intStep, e_fp);
    double fn = std::fabs(mseg::IntegratedKernelClassification::integrateTrapz(intStep, e_fn));
    double tp = ((double) test.size()) - fp;
    double tn = ((double) frameCount) - tp - fp - fn;

    // Prepare output

    mseg::EvaluationResultPtr tpResult = new mseg::EvaluationResult();
    tpResult->name = "tp";
    tpResult->isGroup = false;
    tpResult->score = tp;
    tpResult->isScoreInt = false;

    mseg::EvaluationResultPtr tnResult = new mseg::EvaluationResult();
    tnResult->name = "tn";
    tnResult->isGroup = false;
    tnResult->score = tn;
    tnResult->isScoreInt = false;

    mseg::EvaluationResultPtr fpResult = new mseg::EvaluationResult();
    fpResult->name = "fp";
    fpResult->isGroup = false;
    fpResult->score = fp;
    fpResult->isScoreInt = false;

    mseg::EvaluationResultPtr fnResult = new mseg::EvaluationResult();
    fnResult->name = "fn";
    fnResult->isGroup = false;
    fnResult->score = fn;
    fnResult->isScoreInt = false;

    return std::make_tuple(tpResult, tnResult, fpResult, fnResult);
}

double
mseg::IntegratedKernelClassification::gaussian(double x, double mu, double sig)
{
    double sigSquared = sig * sig;
    double xMinusMu = x - mu;
    double factor = 1.0f / (std::sqrt(2.0f * M_PI * sigSquared));
    double expArg = - (xMinusMu * xMinusMu) / (2.0f * sigSquared);

    return factor * std::exp(expArg);
}

double
mseg::IntegratedKernelClassification::gaussianSum(double x, std::vector<int> keyFrames, int sign)
{
    double result = 0.0f;

    for (int kf : keyFrames)
    {
        result += mseg::IntegratedKernelClassification::gaussian(x, ((double) kf), mseg::IntegratedKernelClassification::sigma);
    }

    return sign * result;
}

std::tuple<std::vector<double>, std::vector<double> >
mseg::IntegratedKernelClassification::splitClassificationError(std::vector<double> e_c)
{
    std::vector<double> e_fp;
    std::vector<double> e_fn;

    for (double v : e_c)
    {
        if (v > 0)
        {
            e_fp.push_back(v);
            e_fn.push_back(0.0f);
        }
        else if (v < 0)
        {
            e_fp.push_back(0.0f);
            e_fn.push_back(v);
        }
        else
        {
            e_fp.push_back(0.0f);
            e_fn.push_back(0.0f);
        }
    }

    return std::make_tuple(e_fp, e_fn);
}

double
mseg::IntegratedKernelClassification::integrateTrapz(double intStep, std::vector<double> y)
{
    // SEE https://en.wikipedia.org/wiki/Trapezoidal_rule FOR FORMULA

    double area;

    unsigned int N = y.size() - 1;
    double h = intStep;

    double sum = 0.0f;

    for (unsigned int k = 0; k < N; k++)
    {
        sum += y[k + 1] + y[k];
    }

    area = (h / 2.0f) * sum;
    return area;
}
