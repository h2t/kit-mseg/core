/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::EvaluationController
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/components/EvaluationController/EvaluationController.h>

// Public constants (Name for root evaluation result object)
const std::string mseg::EvaluationController::EVA_ROOT_NAME = "Evaluation results";

// Keys for this class
const std::string mseg::EvaluationController::K_RESULTS = "Results";
const std::string mseg::EvaluationController::K_RESULTS_OVERALL = "Overall results";
const std::string mseg::EvaluationController::K_INDIVIDUAL_RESULTS = "Individual results";
const std::string mseg::EvaluationController::K_IKM_CLASSIFICATION = "Integrated kernel";
const std::string mseg::EvaluationController::K_MARGIN_CLASSIFICATION = "Margin";
const std::string mseg::EvaluationController::K_STANDARD_CLASSIFICATION = "Standard";
const std::string mseg::EvaluationController::K_RAW_CLASSIFICATION = "Raw data";
const std::string mseg::EvaluationController::K_ROUGH_GRANULARITY = "Rough";
const std::string mseg::EvaluationController::K_MEDIUM_GRANULARITY = "Medium";
const std::string mseg::EvaluationController::K_FINE_GRANULARITY = "Fine";

void
mseg::EvaluationController::onInitComponent()
{
    this->offeringTopic("EvaluationProgress");
}

void
mseg::EvaluationController::onConnectComponent()
{
    this->evaluationProgressTopic = this->getTopic<mseg::EvaluationProgressTopicPrx>("EvaluationProgress");
}

void
mseg::EvaluationController::onDisconnectComponent()
{

}

void
mseg::EvaluationController::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr
mseg::EvaluationController::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new EvaluationControllerPropertyDefinitions(getConfigIdentifier()));
}

mseg::FormattedEvaluationResultPtr
mseg::EvaluationController::evaluateAll(const mseg::SegmentationResultDataset& segResult, const Ice::Current&)
{
    const std::vector<mseg::SegmentationResult> segResults = segResult.results;

    // Init progress
    this->resetProgress((int) segResults.size());

    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();
    result->name = mseg::EvaluationController::EVA_ROOT_NAME;
    result->isGroup = true;
    result->isGroupExpanded = true;
    result->collapseGroup = true;
    result->groupName = "evaluation-results";

    mseg::EvaluationResultPtr totalResult;

    mseg::EvaluationResultPtr individualResults = new mseg::EvaluationResult();
    individualResults->name = mseg::EvaluationController::K_INDIVIDUAL_RESULTS;
    individualResults->isGroup = true;
    individualResults->isGroupExpanded = true;
    individualResults->groupName = "individual-results";

    ARMARX_INFO << "Evaluating all";

    for (const mseg::SegmentationResult& s : segResults)
    {
        mseg::EvaluationResultPtr subResult = new mseg::EvaluationResult();
        subResult->name = s.recording.filename;
        subResult->isGroup = true;
        subResult->isGroupExpanded = (segResults.size() == 1);
        subResult->groupName = "result";
        subResult->groupAttributeName = "motion-recording";
        subResult->groupAttributeValue = s.recording.filename;

        std::vector<std::tuple<int, int> > groundTruth = mseg::Utility::loadGroundTruth(s.recording.path + ".txt");

        // Perform all evaluations once for each significance
        for (int significance = 1; significance <= 3; significance++)
        {
            mseg::EvaluationResultPtr resultForSignificance;

            // Filter ground truth for given significance
            std::vector<int> filteredGroundTruth = mseg::Utility::filterGroundTruthBySignificance(groundTruth, significance);

            // Determine group name
            std::string name;

            if (significance == 3)
            {
                name = mseg::EvaluationController::K_ROUGH_GRANULARITY;
            }
            else if (significance == 2)
            {
                name = mseg::EvaluationController::K_MEDIUM_GRANULARITY;
            }
            else
            {
                name = mseg::EvaluationController::K_FINE_GRANULARITY;
            }

            // Run whole evaluation process for given significance
            resultForSignificance = this->evaluateAllForSignificance(filteredGroundTruth, s.keyFrames, s.recording.frameCount, name);

            subResult->subResults.push_back(resultForSignificance);
        }

        individualResults->subResults.push_back(subResult);
    }

    if (segResults.size() != 1)
    {
        totalResult = this->evaluateTotalResult(individualResults->subResults);

        result->subResults.push_back(totalResult);
    }

    result->subResults.push_back(individualResults);

    ARMARX_INFO << "Evaluated all";

    return this->formatResult(segResult, result);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::evaluateAllForSignificance(std::vector<int> groundTruth, std::vector<int> test, int frameCount, std::string name)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();
    result->name = name + " granularity";
    result->isGroup = true;
    result->isGroupExpanded = true;
    result->groupName = "approaches";
    result->groupAttributeName = "granularity";
    result->groupAttributeValue = name;

    ARMARX_INFO << "Evaluating for given granularity '" << name << "'";

    mseg::EvaluationResultPtr standardClass = this->evaluateWithStandardClassification(groundTruth, test, frameCount);
    mseg::EvaluationResultPtr marginClass = this->evaluateWithMarginClassification(groundTruth, test, frameCount);
    mseg::EvaluationResultPtr ikmClass = this->evaluateWithIntegratedKernelClassification(groundTruth, test, frameCount);
    mseg::EvaluationResultPtr pse = this->evaluateWithPenalisedErrorMetric(groundTruth, test);

    result->subResults.push_back(ikmClass);
    result->subResults.push_back(marginClass);
    result->subResults.push_back(standardClass);
    result->subResults.push_back(pse);

    ARMARX_INFO << "Evaluation for given granularity done";

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::evaluateTotalResult(std::vector<mseg::EvaluationResultPtr> individualResults)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();
    result->name = mseg::EvaluationController::K_RESULTS_OVERALL;
    result->isGroup = true;
    result->isGroupExpanded = true;
    result->groupName = "overall-results";

    // Fine granularity
    {
        mseg::EvaluationResultPtr resultFine = new mseg::EvaluationResult();
        resultFine->name = mseg::EvaluationController::K_FINE_GRANULARITY + " granularity";
        resultFine->isGroup = true;
        resultFine->isGroupExpanded = true;
        resultFine->groupName = "approaches";
        resultFine->groupAttributeName = "granularity";
        resultFine->groupAttributeValue = "Fine";

        // IKM
        {
            mseg::EvaluationResultPtr resultIkm = new mseg::EvaluationResult();
            resultIkm->name = mseg::EvaluationController::K_IKM_CLASSIFICATION + " approach";
            resultIkm->isGroup = true;
            resultIkm->isGroupExpanded = true;
            resultIkm->groupName = "approach";
            resultIkm->groupAttributeName = "name";
            resultIkm->groupAttributeValue = mseg::EvaluationController::K_IKM_CLASSIFICATION;

            // Get raw results
            {
                mseg::EvaluationResultPtr resultRaw = new mseg::EvaluationResult();
                resultRaw->name = mseg::EvaluationController::K_RAW_CLASSIFICATION;
                resultRaw->isGroup = true;
                resultRaw->isGroupExpanded = false;

                mseg::EvaluationResultPtr tp = this->accumulateOverSet(mseg::EvaluationController::K_FINE_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "tp",
                                               individualResults);

                mseg::EvaluationResultPtr tn = this->accumulateOverSet(mseg::EvaluationController::K_FINE_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "tn",
                                               individualResults);

                mseg::EvaluationResultPtr fp = this->accumulateOverSet(mseg::EvaluationController::K_FINE_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "fp",
                                               individualResults);

                mseg::EvaluationResultPtr fn = this->accumulateOverSet(mseg::EvaluationController::K_FINE_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "fn",
                                               individualResults);

                this->reportProgress();

                resultRaw->subResults.push_back(tp);
                resultRaw->subResults.push_back(tn);
                resultRaw->subResults.push_back(fp);
                resultRaw->subResults.push_back(fn);

                resultIkm->subResults.push_back(resultRaw);

                mseg::EvaluationResultPtr resultMetrics = this->evaluateWithGenericClassification(tp, tn, fp, fn);
                resultIkm->subResults.push_back(resultMetrics);
            }

            resultFine->subResults.push_back(resultIkm);
        }

        // Margin
        {
            mseg::EvaluationResultPtr resultMargin = new mseg::EvaluationResult();
            resultMargin->name = mseg::EvaluationController::K_MARGIN_CLASSIFICATION + " approach";
            resultMargin->isGroup = true;
            resultMargin->isGroupExpanded = false;
            resultMargin->groupName = "approach";
            resultMargin->groupAttributeName = "name";
            resultMargin->groupAttributeValue = mseg::EvaluationController::K_MARGIN_CLASSIFICATION;

            // Get raw results
            {
                mseg::EvaluationResultPtr resultRaw = new mseg::EvaluationResult();
                resultRaw->name = mseg::EvaluationController::K_RAW_CLASSIFICATION;
                resultRaw->isGroup = true;
                resultRaw->isGroupExpanded = false;

                mseg::EvaluationResultPtr tp = this->accumulateOverSet(mseg::EvaluationController::K_FINE_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "tp",
                                               individualResults);

                mseg::EvaluationResultPtr tn = this->accumulateOverSet(mseg::EvaluationController::K_FINE_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "tn",
                                               individualResults);

                mseg::EvaluationResultPtr fp = this->accumulateOverSet(mseg::EvaluationController::K_FINE_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "fp",
                                               individualResults);

                mseg::EvaluationResultPtr fn = this->accumulateOverSet(mseg::EvaluationController::K_FINE_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "fn",
                                               individualResults);

                this->reportProgress();

                resultRaw->subResults.push_back(tp);
                resultRaw->subResults.push_back(tn);
                resultRaw->subResults.push_back(fp);
                resultRaw->subResults.push_back(fn);

                resultMargin->subResults.push_back(resultRaw);

                mseg::EvaluationResultPtr resultMetrics = this->evaluateWithGenericClassification(tp, tn, fp, fn);
                resultMargin->subResults.push_back(resultMetrics);
            }

            resultFine->subResults.push_back(resultMargin);
        }

        result->subResults.push_back(resultFine);
    }

    // Medium granularity
    {
        mseg::EvaluationResultPtr resultMedium = new mseg::EvaluationResult();
        resultMedium->name = mseg::EvaluationController::K_MEDIUM_GRANULARITY + " granularity";
        resultMedium->isGroup = true;
        resultMedium->isGroupExpanded = true;
        resultMedium->groupName = "approaches";
        resultMedium->groupAttributeName = "granularity";
        resultMedium->groupAttributeValue = "Medium";

        // IKM
        {
            mseg::EvaluationResultPtr resultIkm = new mseg::EvaluationResult();
            resultIkm->name = mseg::EvaluationController::K_IKM_CLASSIFICATION + " approach";
            resultIkm->isGroup = true;
            resultIkm->isGroupExpanded = true;
            resultIkm->groupName = "approach";
            resultIkm->groupAttributeName = "name";
            resultIkm->groupAttributeValue = mseg::EvaluationController::K_IKM_CLASSIFICATION;

            // Get raw results
            {
                mseg::EvaluationResultPtr resultRaw = new mseg::EvaluationResult();
                resultRaw->name = mseg::EvaluationController::K_RAW_CLASSIFICATION;
                resultRaw->isGroup = true;
                resultRaw->isGroupExpanded = false;

                mseg::EvaluationResultPtr tp = this->accumulateOverSet(mseg::EvaluationController::K_MEDIUM_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "tp",
                                               individualResults);

                mseg::EvaluationResultPtr tn = this->accumulateOverSet(mseg::EvaluationController::K_MEDIUM_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "tn",
                                               individualResults);

                mseg::EvaluationResultPtr fp = this->accumulateOverSet(mseg::EvaluationController::K_MEDIUM_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "fp",
                                               individualResults);

                mseg::EvaluationResultPtr fn = this->accumulateOverSet(mseg::EvaluationController::K_MEDIUM_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "fn",
                                               individualResults);

                this->reportProgress();

                resultRaw->subResults.push_back(tp);
                resultRaw->subResults.push_back(tn);
                resultRaw->subResults.push_back(fp);
                resultRaw->subResults.push_back(fn);

                resultIkm->subResults.push_back(resultRaw);

                mseg::EvaluationResultPtr resultMetrics = this->evaluateWithGenericClassification(tp, tn, fp, fn);
                resultIkm->subResults.push_back(resultMetrics);
            }

            resultMedium->subResults.push_back(resultIkm);
        }

        // Margin
        {
            mseg::EvaluationResultPtr resultMargin = new mseg::EvaluationResult();
            resultMargin->name = mseg::EvaluationController::K_MARGIN_CLASSIFICATION + " approach";
            resultMargin->isGroup = true;
            resultMargin->isGroupExpanded = false;
            resultMargin->groupName = "approach";
            resultMargin->groupAttributeName = "name";
            resultMargin->groupAttributeValue = mseg::EvaluationController::K_MARGIN_CLASSIFICATION;

            // Get raw results
            {
                mseg::EvaluationResultPtr resultRaw = new mseg::EvaluationResult();
                resultRaw->name = mseg::EvaluationController::K_RAW_CLASSIFICATION;
                resultRaw->isGroup = true;
                resultRaw->isGroupExpanded = false;

                mseg::EvaluationResultPtr tp = this->accumulateOverSet(mseg::EvaluationController::K_MEDIUM_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "tp",
                                               individualResults);

                mseg::EvaluationResultPtr tn = this->accumulateOverSet(mseg::EvaluationController::K_MEDIUM_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "tn",
                                               individualResults);

                mseg::EvaluationResultPtr fp = this->accumulateOverSet(mseg::EvaluationController::K_MEDIUM_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "fp",
                                               individualResults);

                mseg::EvaluationResultPtr fn = this->accumulateOverSet(mseg::EvaluationController::K_MEDIUM_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "fn",
                                               individualResults);

                this->reportProgress();

                resultRaw->subResults.push_back(tp);
                resultRaw->subResults.push_back(tn);
                resultRaw->subResults.push_back(fp);
                resultRaw->subResults.push_back(fn);

                resultMargin->subResults.push_back(resultRaw);

                mseg::EvaluationResultPtr resultMetrics = this->evaluateWithGenericClassification(tp, tn, fp, fn);
                resultMargin->subResults.push_back(resultMetrics);
            }

            resultMedium->subResults.push_back(resultMargin);
        }

        result->subResults.push_back(resultMedium);
    }

    // Rough granularity
    {
        mseg::EvaluationResultPtr resultRough = new mseg::EvaluationResult();
        resultRough->name = mseg::EvaluationController::K_ROUGH_GRANULARITY + " granularity";
        resultRough->isGroup = true;
        resultRough->isGroupExpanded = true;
        resultRough->groupName = "approaches";
        resultRough->groupAttributeName = "granularity";
        resultRough->groupAttributeValue = "Rough";

        // IKM
        {
            mseg::EvaluationResultPtr resultIkm = new mseg::EvaluationResult();
            resultIkm->name = mseg::EvaluationController::K_IKM_CLASSIFICATION + " approach";
            resultIkm->isGroup = true;
            resultIkm->isGroupExpanded = true;
            resultIkm->groupName = "approach";
            resultIkm->groupAttributeName = "name";
            resultIkm->groupAttributeValue = mseg::EvaluationController::K_IKM_CLASSIFICATION;

            // Get raw results
            {
                mseg::EvaluationResultPtr resultRaw = new mseg::EvaluationResult();
                resultRaw->name = mseg::EvaluationController::K_RAW_CLASSIFICATION;
                resultRaw->isGroup = true;
                resultRaw->isGroupExpanded = false;

                mseg::EvaluationResultPtr tp = this->accumulateOverSet(mseg::EvaluationController::K_ROUGH_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "tp",
                                               individualResults);

                mseg::EvaluationResultPtr tn = this->accumulateOverSet(mseg::EvaluationController::K_ROUGH_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "tn",
                                               individualResults);

                mseg::EvaluationResultPtr fp = this->accumulateOverSet(mseg::EvaluationController::K_ROUGH_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "fp",
                                               individualResults);

                mseg::EvaluationResultPtr fn = this->accumulateOverSet(mseg::EvaluationController::K_ROUGH_GRANULARITY,
                                               mseg::EvaluationController::K_IKM_CLASSIFICATION,
                                               "fn",
                                               individualResults);

                this->reportProgress();

                resultRaw->subResults.push_back(tp);
                resultRaw->subResults.push_back(tn);
                resultRaw->subResults.push_back(fp);
                resultRaw->subResults.push_back(fn);

                resultIkm->subResults.push_back(resultRaw);

                mseg::EvaluationResultPtr resultMetrics = this->evaluateWithGenericClassification(tp, tn, fp, fn);
                resultIkm->subResults.push_back(resultMetrics);
            }

            resultRough->subResults.push_back(resultIkm);
        }

        // Margin
        {
            mseg::EvaluationResultPtr resultMargin = new mseg::EvaluationResult();
            resultMargin->name = mseg::EvaluationController::K_MARGIN_CLASSIFICATION + " approach";
            resultMargin->isGroup = true;
            resultMargin->isGroupExpanded = false;
            resultMargin->groupName = "approach";
            resultMargin->groupAttributeName = "name";
            resultMargin->groupAttributeValue = mseg::EvaluationController::K_MARGIN_CLASSIFICATION;

            // Get raw results
            {
                mseg::EvaluationResultPtr resultRaw = new mseg::EvaluationResult();
                resultRaw->name = mseg::EvaluationController::K_RAW_CLASSIFICATION;
                resultRaw->isGroup = true;
                resultRaw->isGroupExpanded = false;

                mseg::EvaluationResultPtr tp = this->accumulateOverSet(mseg::EvaluationController::K_ROUGH_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "tp",
                                               individualResults);

                mseg::EvaluationResultPtr tn = this->accumulateOverSet(mseg::EvaluationController::K_ROUGH_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "tn",
                                               individualResults);

                mseg::EvaluationResultPtr fp = this->accumulateOverSet(mseg::EvaluationController::K_ROUGH_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "fp",
                                               individualResults);

                mseg::EvaluationResultPtr fn = this->accumulateOverSet(mseg::EvaluationController::K_ROUGH_GRANULARITY,
                                               mseg::EvaluationController::K_MARGIN_CLASSIFICATION,
                                               "fn",
                                               individualResults);

                this->reportProgress();

                resultRaw->subResults.push_back(tp);
                resultRaw->subResults.push_back(tn);
                resultRaw->subResults.push_back(fp);
                resultRaw->subResults.push_back(fn);

                resultMargin->subResults.push_back(resultRaw);

                mseg::EvaluationResultPtr resultMetrics = this->evaluateWithGenericClassification(tp, tn, fp, fn);
                resultMargin->subResults.push_back(resultMetrics);
            }

            resultRough->subResults.push_back(resultMargin);
        }

        result->subResults.push_back(resultRough);
    }

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::evaluateWithStandardClassification(std::vector<int> groundTruth, std::vector<int> test, int frameCount)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();
    result->name = mseg::EvaluationController::K_STANDARD_CLASSIFICATION + " approach";
    result->isGroup = true;
    result->isGroupExpanded = false;
    result->groupName = "approach";
    result->groupAttributeName = "name";
    result->groupAttributeValue = mseg::EvaluationController::K_STANDARD_CLASSIFICATION;

    mseg::EvaluationResultPtr tp;
    mseg::EvaluationResultPtr tn;
    mseg::EvaluationResultPtr fp;
    mseg::EvaluationResultPtr fn;

    ARMARX_INFO << "Evaluating using standard classfication";

    std::tie(tp, tn, fp, fn) = mseg::StandardClassification::getClassification(groundTruth, test, frameCount);

    this->reportProgress();

    mseg::EvaluationResultPtr classification = new mseg::EvaluationResult();
    classification->name = mseg::EvaluationController::K_RAW_CLASSIFICATION;
    classification->isGroup = true;
    classification->isGroupExpanded = false;

    classification->subResults.push_back(tp);
    classification->subResults.push_back(tn);
    classification->subResults.push_back(fp);
    classification->subResults.push_back(fn);

    result->subResults.push_back(classification);

    mseg::EvaluationResultPtr resultMetrics = this->evaluateWithGenericClassification(tp, tn, fp, fn);
    result->subResults.push_back(resultMetrics);

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::evaluateWithMarginClassification(std::vector<int> groundTruth, std::vector<int> test, int frameCount)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();
    result->name = mseg::EvaluationController::K_MARGIN_CLASSIFICATION + " approach";
    result->isGroup = true;
    result->isGroupExpanded = false;
    result->groupName = "approach";
    result->groupAttributeName = "name";
    result->groupAttributeValue = mseg::EvaluationController::K_MARGIN_CLASSIFICATION;

    mseg::EvaluationResultPtr tp;
    mseg::EvaluationResultPtr tn;
    mseg::EvaluationResultPtr fp;
    mseg::EvaluationResultPtr fn;

    ARMARX_INFO << "Evaluating using margin approach";

    std::tie(tp, tn, fp, fn) = mseg::MarginClassification::getClassification(groundTruth, test, frameCount);

    this->reportProgress();

    mseg::EvaluationResultPtr classification = new mseg::EvaluationResult();
    classification->name = mseg::EvaluationController::K_RAW_CLASSIFICATION;
    classification->isGroup = true;
    classification->isGroupExpanded = false;

    classification->subResults.push_back(tp);
    classification->subResults.push_back(tn);
    classification->subResults.push_back(fp);
    classification->subResults.push_back(fn);

    result->subResults.push_back(classification);

    mseg::EvaluationResultPtr resultMetrics = this->evaluateWithGenericClassification(tp, tn, fp, fn);
    result->subResults.push_back(resultMetrics);

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::evaluateWithIntegratedKernelClassification(std::vector<int> groundTruth, std::vector<int> test, int frameCount)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();
    result->name = mseg::EvaluationController::K_IKM_CLASSIFICATION + " approach";
    result->isGroup = true;
    result->isGroupExpanded = true;
    result->groupName = "approach";
    result->groupAttributeName = "name";
    result->groupAttributeValue = mseg::EvaluationController::K_IKM_CLASSIFICATION;

    mseg::EvaluationResultPtr tp;
    mseg::EvaluationResultPtr tn;
    mseg::EvaluationResultPtr fp;
    mseg::EvaluationResultPtr fn;

    ARMARX_INFO << "Evaluating using IKM classificiation";

    std::tie(tp, tn, fp, fn) = mseg::IntegratedKernelClassification::getClassification(groundTruth, test, frameCount);

    this->reportProgress();

    mseg::EvaluationResultPtr classification = new mseg::EvaluationResult();
    classification->name = mseg::EvaluationController::K_RAW_CLASSIFICATION;
    classification->isGroup = true;
    classification->isGroupExpanded = false;

    classification->subResults.push_back(tp);
    classification->subResults.push_back(tn);
    classification->subResults.push_back(fp);
    classification->subResults.push_back(fn);

    result->subResults.push_back(classification);

    mseg::EvaluationResultPtr resultMetrics = this->evaluateWithGenericClassification(tp, tn, fp, fn);
    result->subResults.push_back(resultMetrics);

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::evaluateWithPenalisedErrorMetric(std::vector<int> groundTruth, std::vector<int> test)
{
    ARMARX_INFO << "Evaluating using Penalised Squared Error";

    mseg::EvaluationResultPtr result = mseg::PenalisedErrorMetric::evaluate(groundTruth, test);

    this->reportProgress();

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::evaluateWithGenericClassification(mseg::EvaluationResultPtr tp, mseg::EvaluationResultPtr tn, mseg::EvaluationResultPtr fp, mseg::EvaluationResultPtr fn)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();
    result->isGroup = true;
    result->collapseGroup = true;
    result->groupName = "scores";

    result->subResults.push_back(this->precision(tp, fp));
    this->reportProgress();

    result->subResults.push_back(this->recall(tp, fn));
    this->reportProgress();

    result->subResults.push_back(this->accuracy(tp, tn, fp, fn));
    this->reportProgress();

    result->subResults.push_back(this->f1Score(tp, fp, fn));
    this->reportProgress();

    result->subResults.push_back(this->classificationF1Score(tp, tn, fp, fn));
    this->reportProgress();

    result->subResults.push_back(this->mccScore(tp, tn, fp, fn));
    this->reportProgress();

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::accumulateOverSet(std::string significance, std::string classificationMethod, std::string classification, std::vector<mseg::EvaluationResultPtr> resFiles)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();
    result->name = classification;
    result->isGroup = false;
    result->score = 0.0f;
    result->isScoreInt = true;

    if (classificationMethod == mseg::EvaluationController::K_IKM_CLASSIFICATION)
    {
        result->isScoreInt = false;
    }

    for (mseg::EvaluationResultPtr resFile : resFiles)
    {
        for (mseg::EvaluationResultPtr resSignificance : resFile->subResults)
        {
            if (resSignificance->groupAttributeValue != significance)
            {
                continue;
            }

            for (mseg::EvaluationResultPtr resClassificationMethod : resSignificance->subResults)
            {
                if (resClassificationMethod->groupAttributeValue != classificationMethod)
                {
                    continue;
                }

                for (mseg::EvaluationResultPtr resRawClassification : resClassificationMethod->subResults)
                {
                    if (resRawClassification->name != mseg::EvaluationController::K_RAW_CLASSIFICATION)
                    {
                        continue;
                    }

                    for (mseg::EvaluationResultPtr resClassification : resRawClassification->subResults)
                    {
                        if (resClassification->name != classification)
                        {
                            continue;
                        }

                        result->score += resClassification->score;
                    }

                    break;
                }

                break;
            }

            break;
        }
    }

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::precision(double tp, double fp)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();

    result->name = "Precision";
    result->score = tp / (tp + fp);
    result->isScoreInt = false;
    result->groupName = "score";
    result->groupAttributeName = "name";
    result->groupAttributeValue = result->name;

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::recall(double tp, double fn)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();

    result->name = "Recall";
    result->score = tp / (tp + fn);
    result->isScoreInt = false;
    result->groupName = "score";
    result->groupAttributeName = "name";
    result->groupAttributeValue = result->name;

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::accuracy(double tp, double tn, double fp, double fn)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();

    result->name = "Accuracy";
    result->score = (tp + tn) / (tp + tn + fp + fn);
    result->isScoreInt = false;
    result->groupName = "score";
    result->groupAttributeName = "name";
    result->groupAttributeValue = result->name;

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::f1Score(double precision, double recall)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();

    result->name = "F_1 score";
    result->score = 2 * ((precision * recall) / (precision + recall));
    result->isScoreInt = false;
    result->groupName = "score";
    result->groupAttributeName = "name";
    result->groupAttributeValue = "F_1";

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::f1Score(double tp, double fp, double fn)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();

    result->name = "F_1 score";
    result->score = (2 * tp) / ((2 * tp) + fp + fn);
    result->isScoreInt = false;
    result->groupName = "score";
    result->groupAttributeName = "name";
    result->groupAttributeValue = "F_1";

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::classificationF1Score(double tp, double tn, double fp, double fn)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();

    result->name = "Classification F_1 score";
    result->score = (2 * (tp + tn)) / ((2 * (tp + tn)) + fp + fn);
    result->isScoreInt = false;
    result->groupName = "score";
    result->groupAttributeName = "name";
    result->groupAttributeValue = "Classification F_1";

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::mccScore(double tp, double tn, double fp, double fn)
{
    mseg::EvaluationResultPtr result = new mseg::EvaluationResult();

    result->name = "MCC";
    result->score = ((tp * tn) - (fp * fn)) / (std::sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn)));
    result->isScoreInt = false;
    result->groupName = "score";
    result->groupAttributeName = "name";
    result->groupAttributeValue = result->name;

    return result;
}

mseg::EvaluationResultPtr
mseg::EvaluationController::precision(int tp, int fp)
{
    return this->precision((double) tp, (double) fp);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::recall(int tp, int fn)
{
    return this->recall((double) tp, (double) fn);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::accuracy(int tp, int tn, int fp, int fn)
{
    return this->accuracy((double) tp, (double) tn, (double) fp, (double) fn);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::f1Score(int tp, int fp, int fn)
{
    return this->f1Score((double) tp, (double) fp, (double) fn);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::classificationF1Score(int tp, int tn, int fp, int fn)
{
    return this->classificationF1Score((double) tp, (double) tn, (double) fp, (double) fn);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::mccScore(int tp, int tn, int fp, int fn)
{
    return this->mccScore((double) tp, (double) tn, (double) fp, (double) fn);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::precision(mseg::EvaluationResultPtr tp, mseg::EvaluationResultPtr fp)
{
    return this->precision(tp->score, fp->score);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::recall(mseg::EvaluationResultPtr tp, mseg::EvaluationResultPtr fn)
{
    return this->recall(tp->score, fn->score);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::accuracy(mseg::EvaluationResultPtr tp, mseg::EvaluationResultPtr tn, mseg::EvaluationResultPtr fp, mseg::EvaluationResultPtr fn)
{
    return this->accuracy(tp->score, tn->score, fp->score, fn->score);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::f1Score(mseg::EvaluationResultPtr precision, mseg::EvaluationResultPtr recall)
{
    return this->f1Score(precision->score, recall->score);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::f1Score(mseg::EvaluationResultPtr tp, mseg::EvaluationResultPtr fp, mseg::EvaluationResultPtr fn)
{
    return this->f1Score(tp->score, fp->score, fn->score);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::classificationF1Score(mseg::EvaluationResultPtr tp, mseg::EvaluationResultPtr tn, mseg::EvaluationResultPtr fp, mseg::EvaluationResultPtr fn)
{
    return this->classificationF1Score(tp->score, tn->score, fp->score, fn->score);
}

mseg::EvaluationResultPtr
mseg::EvaluationController::mccScore(mseg::EvaluationResultPtr tp, mseg::EvaluationResultPtr tn, mseg::EvaluationResultPtr fp, mseg::EvaluationResultPtr fn)
{
    return this->mccScore(tp->score, tn->score, fp->score, fn->score);
}

mseg::FormattedEvaluationResultPtr
mseg::EvaluationController::formatResult(const mseg::SegmentationResultDataset& segResult, mseg::EvaluationResultPtr evaResult)
{
    // Format evaluation results
    mseg::FormattedEvaluationResultPtr result = this->toFormattedEvaluationResult(evaResult);

    // Add Meta information
    {
        mseg::FormattedEvaluationResultPtr meta = new mseg::FormattedEvaluationResult();
        meta->name = "Meta information";
        meta->isGroup = true;
        meta->isGroupExpanded = false;

        // MSeg Version
        {
            mseg::FormattedEvaluationResultPtr r = new mseg::FormattedEvaluationResult();
            r->name = "MSeg Version";
            r->value = mseg::Utility::getVersion();
            r->type = mseg::VarTypes::StringType;

            meta->subResults.push_back(r);
        }

        // Dataset name + version (implicit)
        {
            mseg::FormattedEvaluationResultPtr r = new mseg::FormattedEvaluationResult();
            r->name = "Dataset";
            r->value = segResult.datasetName;
            r->type = mseg::VarTypes::StringType;

            meta->subResults.push_back(r);
        }

        // Algorithm name
        {
            mseg::FormattedEvaluationResultPtr r = new mseg::FormattedEvaluationResult();
            r->name = "Algorithm";
            r->value = segResult.algorithmName;
            r->type = mseg::VarTypes::StringType;

            meta->subResults.push_back(r);
        }

        // Required training
        {
            mseg::FormattedEvaluationResultPtr r = new mseg::FormattedEvaluationResult();
            r->name = "Trained";
            r->value = segResult.trainingRequired ? "Yes" : "No";
            r->type = mseg::VarTypes::BoolType;

            meta->subResults.push_back(r);
        }

        // Fetched online
        {
            mseg::FormattedEvaluationResultPtr r = new mseg::FormattedEvaluationResult();
            r->name = "Online";
            r->value = segResult.onlineFetched ? "Yes" : "No";
            r->type = mseg::VarTypes::BoolType;

            meta->subResults.push_back(r);
        }

        // Parameters
        {
            mseg::FormattedEvaluationResultPtr algParams = new mseg::FormattedEvaluationResult();
            algParams->name = "Algorithm parameters";
            algParams->isGroup = true;
            algParams->isGroupExpanded = true;

            for (mseg::AlgorithmParameter param : segResult.parameters)
            {
                mseg::FormattedEvaluationResultPtr r = new mseg::FormattedEvaluationResult();
                r->name = param.name;
                r->value = param.value;
                r->type = param.type;
                r->groupName = "parameter";
                r->groupAttributeName = "name";
                r->groupAttributeValue = param.name;

                algParams->subResults.push_back(r);
            }

            meta->subResults.push_back(algParams);
        }

        // Segmentations
        {
            mseg::FormattedEvaluationResultPtr segs = new mseg::FormattedEvaluationResult();
            segs->name = "Raw segmentation data";
            segs->isGroup = true;
            segs->isGroupExpanded = true;

            for (mseg::SegmentationResult s : segResult.results)
            {
                mseg::FormattedEvaluationResultPtr mr = new mseg::FormattedEvaluationResult();
                mr->name = s.recording.filename;
                mr->isGroup = true;
                mr->isGroupExpanded = false;
                mr->groupName = "segmentation-data";
                mr->groupAttributeName = "for";
                mr->groupAttributeValue = s.recording.filename;

                std::vector<std::tuple<int, int> > gt = mseg::Utility::loadGroundTruth(s.recording.path + ".txt");
                std::vector<int> rough = mseg::Utility::filterGroundTruthBySignificance(gt, 3);
                std::vector<int> medium = mseg::Utility::filterGroundTruthBySignificance(gt, 2);
                std::vector<int> fine = mseg::Utility::filterGroundTruthBySignificance(gt, 1);

                // Ground Truths
                {
                    mseg::FormattedEvaluationResultPtr gts = new mseg::FormattedEvaluationResult();
                    gts->name = "Ground truths";
                    gts->isGroup = true;
                    gts->isGroupExpanded = true;
                    gts->groupName = "ground-truths";

                    // Rough ground truth
                    {
                        mseg::FormattedEvaluationResultPtr r = new mseg::FormattedEvaluationResult();
                        r->name = "Rough ground truth";
                        r->value = this->vectorIntToString(rough);
                        r->type = mseg::VarTypes::JsonType;
                        r->groupName = "ground-truth";
                        r->groupAttributeName = "granularity";
                        r->groupAttributeValue = "Rough";

                        gts->subResults.push_back(r);
                    }

                    // Medium ground truth
                    {
                        mseg::FormattedEvaluationResultPtr r = new mseg::FormattedEvaluationResult();
                        r->name = "Medium ground truth";
                        r->value = this->vectorIntToString(medium);
                        r->type = mseg::VarTypes::JsonType;
                        r->groupName = "ground-truth";
                        r->groupAttributeName = "granularity";
                        r->groupAttributeValue = "Medium";

                        gts->subResults.push_back(r);
                    }

                    // Fine ground truth
                    {
                        mseg::FormattedEvaluationResultPtr r = new mseg::FormattedEvaluationResult();
                        r->name = "Fine ground truth";
                        r->value = this->vectorIntToString(fine);
                        r->type = mseg::VarTypes::JsonType;
                        r->groupName = "ground-truth";
                        r->groupAttributeName = "granularity";
                        r->groupAttributeValue = "Fine";

                        gts->subResults.push_back(r);
                    }

                    mr->subResults.push_back(gts);
                }

                // Actual segmentation
                {
                    mseg::FormattedEvaluationResultPtr r = new mseg::FormattedEvaluationResult();
                    r->name = "Segmentation";
                    r->value = this->vectorIntToString(s.keyFrames);
                    r->type = mseg::VarTypes::JsonType;
                    r->groupName = "segmentation";

                    mr->subResults.push_back(r);
                }

                segs->subResults.push_back(mr);
            }

            meta->subResults.push_back(segs);
        }

        result->subResults.push_back(meta);
    }

    return result;
}

mseg::FormattedEvaluationResultPtr
mseg::EvaluationController::toFormattedEvaluationResult(mseg::EvaluationResultPtr result)
{
    mseg::FormattedEvaluationResultPtr fresult = new mseg::FormattedEvaluationResult();
    fresult->name = result->name;
    fresult->isGroup = result->isGroup;
    fresult->isGroupExpanded = result->isGroupExpanded;
    fresult->collapseGroup = result->collapseGroup;
    fresult->groupName = result->groupName;
    fresult->groupAttributeName = result->groupAttributeName;
    fresult->groupAttributeValue = result->groupAttributeValue;

    if (fresult->isGroup)
    {
        for (mseg::EvaluationResultPtr r : result->subResults)
        {
            fresult->subResults.push_back(this->toFormattedEvaluationResult(r));
        }
    }
    else
    {
        if (result->isScoreInt)
        {
            fresult->value = std::to_string((int) result->score);
            fresult->type = mseg::VarTypes::IntType;
        }
        else
        {
            std::ostringstream ss;
            ss << std::fixed << std::setprecision(3);
            ss << result->score;

            fresult->value = ss.str();
            fresult->type = mseg::VarTypes::FloatType;
        }
    }

    return fresult;
}

std::string
mseg::EvaluationController::vectorIntToString(std::vector<int> pts)
{
    if (pts.size() == 0)
    {
        return "[ ]";
    }

    if (pts.size() == 1)
    {
        return "[ " + std::to_string(pts[0]) + " ]";
    }

    std::string result = "[ ";

    for (int i = 0; i < (int) pts.size() - 1; i++)
    {
        result += std::to_string(pts[i]) + ", ";
    }

    result += std::to_string(pts.back()) + " ]";

    return result;
}


void
mseg::EvaluationController::resetProgress(int segmentationResultsCount)
{
    this->currentStep = 0;
    this->percentage = 0;

    int totalResult = 0;

    if (segmentationResultsCount > 1)
    {
        totalResult = this->stepsForTotalResult;
    }

    this->totalSteps = (segmentationResultsCount * 3 * this->stepPerSignificance) + totalResult;

    this->evaluationProgressTopic->reportEvaluationProgress(0);
}

void
mseg::EvaluationController::reportProgress()
{
    this->currentStep++;

    int newPercentage = (int)(((float) this->currentStep / (float) this->totalSteps) * 100.0f);

    if (this->percentage < newPercentage)
    {
        this->percentage = newPercentage;

        this->evaluationProgressTopic->reportEvaluationProgress(this->percentage);
    }
}
