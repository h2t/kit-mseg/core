armarx_component_set_name("DataExchange")

#find_package(MyLib QUIET)
#armarx_build_if(MyLib_FOUND "MyLib not available")
#
# all include_directories must be guarded by if(Xyz_FOUND)
# for multiple libraries write: if(X_FOUND AND Y_FOUND)....
#if(MyLib_FOUND)
#    include_directories(${MyLib_INCLUDE_DIRS})
#endif()

find_package("Eigen3" QUIET)
find_package("MMMCore" REQUIRED)

if(Eigen3_FOUND)
    include_directories(${Eigen3_INCLUDE_DIR})
endif()

find_package(mseginterfacescpp REQUIRED)
if(mseginterfacescpp_FOUND)
    include_directories(${mseginterfacescpp_INCLUDE_DIRS})
endif()

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore MMMCore mseginterfacescpp Utility)

set(SOURCES
./DataExchange.cpp
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
)
set(HEADERS
./DataExchange.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
)

armarx_add_component("${SOURCES}" "${HEADERS}")
