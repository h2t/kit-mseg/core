/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::SegmentationController
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_MSeg_SegmentationController_H
#define _ARMARX_COMPONENT_MSeg_SegmentationController_H

#include <climits> // INT_MIN INT_MAX
#include <cfloat> // FLT_MIN FLT_MAX
#include <vector>

#include <boost/filesystem.hpp>

#include <IceGrid/Query.h>

#include <ArmarXCore/core/Component.h>

#include <jsoncpp/json/json.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>

#include <MSeg/components/Utility/Utility.h>
#include <MSeg/interface/DataTypes.h>
#include <MSeg/interface/exceptions.h>
#include <MSeg/interface/Topics.h>
#include <MSeg/interface/DataExchangeInterface.h>
#include <MSeg/interface/SegmentationControllerInterface.h>
#include <MSeg/interface/SegmentationAlgorithmInterface.h>

namespace mseg
{
    /**
     * @class SegmentationControllerPropertyDefinitions
     * @brief
     */
    class SegmentationControllerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SegmentationControllerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            this->defineOptionalProperty<std::string>("SegmentationController", "SegmentationController", "MSeg segmentation client");
        }
    };

    /**
     * @defgroup Component-SegmentationController SegmentationController
     * @ingroup MSeg-Components
     * A description of the component SegmentationController.
     *
     * @class SegmentationController
     * @ingroup Component-SegmentationController
     * @brief Brief description of class SegmentationController.
     *
     * Detailed description of class SegmentationController.
     */
    class SegmentationController :
        virtual public armarx::Component,
        virtual public mseg::SegmentationControllerInterface
    {

    private:

        mseg::DataExchangeInterfacePrx data;
        mseg::MotionRecordingDataset dataset;
        std::vector<mseg::AlgorithmParameter> algorithmParameters;
        uint kCrossValidate = 5; // k-fold validation
        int segmentationProgressPercentage = 0;
        int frameCountTotal = 0;
        int frameCountTotalFinished = 0;
        mseg::SegmentationProgressTopicPrx segmentationProgressTopic;

    public:

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "SegmentationController";
        }

    protected:

        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

    private:

        std::vector<std::vector<mseg::MotionRecording> > splitDataset(std::vector<mseg::MotionRecording> dataset, int k);

    public: // SegmentationController interface

        void setDataset(const mseg::MotionRecordingDataset& dataset, const Ice::Current& c = ::Ice::Current());
        virtual std::vector<mseg::AlgorithmParameter> getAlgorithmParameters(const std::string&, const Ice::Current& c = ::Ice::Current());
        virtual void setAlgorithmParameters(const std::string&, const std::vector<mseg::AlgorithmParameter>&, const Ice::Current& c = ::Ice::Current());
        virtual mseg::SegmentationResultDataset segmentDataset(const std::string&, const Ice::Current& c = ::Ice::Current());
        virtual std::vector<std::string> querySegmentationAlgorithms(const Ice::Current& c = ::Ice::Current());

    public: // SegmentationProgressIndividualTopic interface

        void reportSegmentationProgressIndividual(int currentFrame, const Ice::Current& c = ::Ice::Current());

    };
}

#endif
