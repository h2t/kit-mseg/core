/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::Utility
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/components/Utility/Utility.h>

/**
 * C-Style API's for mseg-tools
 *
 * To avoid redundancy, mseg-tools can use these
 * API's with a Python standard library.
 */
extern "C" {
    char const* getVersion()
    {
        return mseg::Utility::getVersion().c_str();
    }

    char const* getMSegHomePath()
    {
        return mseg::Utility::paths::getMSegHomePath().c_str();
    }

    char const* getMotionDataPath()
    {
        return mseg::Utility::paths::getMotionDataPath().c_str();
    }
}

std::string
mseg::Utility::getVersion()
{
    std::string version;

    version = std::to_string(mseg::Utility::MSEG_VERSION_MAJOR) + "." + std::to_string(mseg::Utility::MSEG_VERSION_MINOR) + "." + std::to_string(mseg::Utility::MSEG_VERSION_PATCH);

    if (mseg::Utility::MSEG_RELEASE_TYPE != mseg::Utility::ReleaseType::RELEASE)
    {
        std::string releaseType;

        switch (mseg::Utility::MSEG_RELEASE_TYPE)
        {
            case mseg::Utility::ReleaseType::ALPHA:
                releaseType = "alpha" + std::to_string(mseg::Utility::MSEG_VERSION_APPENDIX);
                break;
            case mseg::Utility::ReleaseType::BETA:
                releaseType = "beta" + std::to_string(mseg::Utility::MSEG_VERSION_APPENDIX);
                break;
            case mseg::Utility::ReleaseType::RELEASE_CANDIDATE:
                releaseType = "rc" + std::to_string(mseg::Utility::MSEG_VERSION_APPENDIX);
                break;
            case mseg::Utility::ReleaseType::SNAPSHOT:
            default:
                releaseType = "SNAPSHOT";
                break;
        }

        version += "-" + releaseType;
    }

    return version;
}

std::string
mseg::Utility::readFile(std::string path)
{
    std::string content = "";
    std::string line;
    std::ifstream myfile(path);

    if (myfile.is_open())
    {
        while (std::getline(myfile, line))
        {
            content += line + '\n';
        }

        myfile.close();
    }

    return content;
}

void
mseg::Utility::writeFile(std::string path, std::string file, std::string content)
{
    mseg::Utility::mkdir(path);

    if (path.back() != '/')
    {
        path += '/';
    }

    std::ofstream write;
    write.open(path + file);
    write << content;
    write.close();
}

void mseg::Utility::deleteFile(std::string path, std::string file)
{
    if (path.back() != '/')
    {
        path += '/';
    }

    std::remove((path + file).c_str());
}

std::vector<std::string>
mseg::Utility::filesInFolder(std::string folder)
{
    boost::filesystem::path p(folder);

    boost::filesystem::directory_iterator end_itr;

    std::vector<std::string> files;

    // cycle through the directory
    for (boost::filesystem::directory_iterator itr(p); itr != end_itr; ++itr)
    {
        // If it's not a directory, list it.
        if (boost::filesystem::is_regular_file(itr->path()))
        {
            // assign current file name and push back.
            std::string current_file = itr->path().string();
            files.push_back(current_file);
        }
    }

    return files;
}

void
mseg::Utility::mkdir(std::string path, std::vector<std::string> subfolders)
{
    std::string fullpath = path + boost::algorithm::join(subfolders, "/");

    bool directory_created = boost::filesystem::create_directories(boost::filesystem::path(fullpath));

    if (directory_created)
    {
        ARMARX_INFO << "Created directory " << fullpath;
    }
}

std::vector<std::tuple<int, int> >
mseg::Utility::loadGroundTruth(std::string path)
{
    std::vector<std::tuple<int, int> > output;

    std::string segFrames = mseg::Utility::readFile(path);

    std::vector<std::string> groundTruthPoints;
    boost::split(groundTruthPoints, segFrames, boost::is_any_of(";"));

    if (groundTruthPoints.size() > 0)
    {
        groundTruthPoints.pop_back();
    }

    for (std::string groundTruthPointRaw : groundTruthPoints)
    {
        std::vector<std::string> groundTruthPoint;
        boost::split(groundTruthPoint, groundTruthPointRaw, boost::is_any_of(","));

        int frame = std::stoi(groundTruthPoint[0]);
        int significance = std::stoi(groundTruthPoint[1]);

        output.push_back(std::make_tuple(frame, significance));
    }

    return output;
}

std::vector<int>
mseg::Utility::filterGroundTruthBySignificance(std::vector<std::tuple<int, int> > groundTruth, int significance)
{
    std::vector<int> output;

    for (auto groundTruthPoint : groundTruth)
    {
        int gtpFrame;
        int gtpSignificance;

        std::tie(gtpFrame, gtpSignificance) = groundTruthPoint;

        if (gtpSignificance >= significance)
        {
            output.push_back(gtpFrame);
        }
    }

    std::sort(output.begin(), output.end());

    return output;
}

std::string
mseg::Utility::getEnv(std::string key)
{
    char const* val = std::getenv(key.c_str());
    return val == NULL ? std::string() : std::string(val);
}

std::string
mseg::Utility::paths::getMSegHomePath(std::vector<std::string> subfolders)
{
    std::string home = mseg::Utility::getEnv("HOME");

    if (home.back() != '/')
    {
        home += '/';
    }

    return home + ".mseg/" + boost::algorithm::join(subfolders, "/") + '/';
}

std::string
mseg::Utility::paths::getMotionDataPath(std::vector<std::string> subfolders)
{
    std::vector<std::string> subfoldersTotal = {"motion-data"};
    subfoldersTotal.insert(subfoldersTotal.end(), subfolders.begin(), subfolders.end());
    return mseg::Utility::paths::getMSegHomePath(subfoldersTotal);
}

std::string
mseg::Utility::paths::getMMMDataPath(std::vector<std::string> subfolders)
{
    std::vector<std::string> subfoldersTotal = {".mmm-data"};
    subfoldersTotal.insert(subfoldersTotal.end(), subfolders.begin(), subfolders.end());
    return mseg::Utility::paths::getMotionDataPath(subfoldersTotal);
}

std::string
mseg::Utility::paths::getMSegConfigPath(std::vector<std::string> subfolders)
{
    std::vector<std::string> subfoldersTotal = {"config"};
    subfoldersTotal.insert(subfoldersTotal.end(), subfolders.begin(), subfolders.end());
    return mseg::Utility::paths::getMSegHomePath(subfoldersTotal);
}
