/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::gui-plugins::EvaluationWidget
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_ArmarXCore_MSeg_EvaluationWidget_H
#define _ARMARX_ArmarXCore_MSeg_EvaluationWidget_H

#include <string>
#include <tuple>

#include <QDateTime>
#include <QFileDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLayout>
#include <QPushButton>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QTreeView>
#include <QWidget>

#include <tinyxml.h> // ToDo: Move to Utility
#include <boost/algorithm/string.hpp>            // for is_any_of // ToDo: Move to Utility
#include <boost/range/algorithm/replace_if.hpp>  // for replace_if // ToDo: Move to Utility
#include <jsoncpp/json/json.h> // ToDo: Move to Utility
#include <jsoncpp/json/reader.h> // ToDo: Move to Utility
#include <jsoncpp/json/writer.h> // ToDo: Move to Utility

#include <ArmarXCore/core/logging/Logging.h>

#include <MSeg/interface/DataTypes.h>

namespace mseg
{
    class EvaluationResultsWidget
        : public QWidget
    {

        Q_OBJECT

    protected:

        QPushButton* buttonExportAsXml;
        QPushButton* buttonExportAsJson;
        QTreeView* treeEvaluationResults;
        QStandardItemModel* modelEvaluationResults;
        mseg::FormattedEvaluationResultPtr evaluationResults;

    public:

        EvaluationResultsWidget(QWidget*);

        virtual ~EvaluationResultsWidget();

        void clearEvaluationResults();
        void updateEvaluationResults(mseg::FormattedEvaluationResultPtr);

    public slots:

        void onButtonExportAsXmlClicked();
        void onButtonExportAsJsonClicked();

    protected:

        virtual void updateEvaluationResultsHierarchy(mseg::FormattedEvaluationResultPtr, QStandardItem*);
        virtual void addNameValue(std::string name, std::string value, QStandardItem* root);
        virtual void addNameValue(QStandardItem* name, QStandardItem* value, QStandardItem* root);

        void exportAsXml(mseg::FormattedEvaluationResultPtr, std::string);
        TiXmlElement* evaluationResultToXmlElement(mseg::FormattedEvaluationResultPtr);

    };
}

#endif
