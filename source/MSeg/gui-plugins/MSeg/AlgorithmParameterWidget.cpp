/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::gui-plugins::AlgorithmParameterWidget
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/gui-plugins/MSeg/AlgorithmParameterWidget.h>

mseg::AlgorithmParameterWidget::AlgorithmParameterWidget(mseg::AlgorithmParameter algorithmParameter)
{
    this->algorithmParameter = algorithmParameter;

    this->setLayout(new QHBoxLayout());
    this->layout()->setSpacing(0);
    this->layout()->setMargin(0);

    this->initSubWidget();
}

mseg::AlgorithmParameterWidget::~AlgorithmParameterWidget()
{

}

void
mseg::AlgorithmParameterWidget::initSubWidget()
{
    if (this->algorithmParameter.value == "")
    {
        this->algorithmParameter.value = this->algorithmParameter.defaultValue;
    }

    switch (this->algorithmParameter.type)
    {
        case mseg::VarTypes::BoolType:
            this->widget = this->addBoolParameterWidget();
            break;

        case mseg::VarTypes::IntType:
            this->widget = this->addIntParameterWidget();
            break;

        case mseg::VarTypes::FloatType:
            this->widget = this->addFloatParameterWidget();
            break;

        case mseg::VarTypes::StringType:
            this->widget = this->addStringParameterWidget();
            break;

        case mseg::VarTypes::JsonType:
            this->widget = this->addJsonParameterWidget();
            break;

        default:
            throw "Unkown data type for algorithm parameter";
            break;
    }

    this->layout()->addWidget(this->widget);
}

mseg::AlgorithmParameter
mseg::AlgorithmParameterWidget::getAlgorithmParameter()
{
    switch (this->algorithmParameter.type)
    {
        case mseg::VarTypes::BoolType:
            this->algorithmParameter.value = (((QCheckBox*) this->widget)->isChecked()) ? "true" : "false";
            break;

        case mseg::VarTypes::IntType:
            this->algorithmParameter.value = std::to_string(((QSpinBox*) this->widget)->value());
            break;

        case mseg::VarTypes::FloatType:
            this->algorithmParameter.value = this->floatToString(((QDoubleSpinBox*) this->widget)->value());
            break;

        case mseg::VarTypes::StringType:
            this->algorithmParameter.value = ((QLineEdit*) this->widget)->text().toStdString();
            break;

        case mseg::VarTypes::JsonType:
            this->algorithmParameter.value = ((QPlainTextEdit*) this->widget)->toPlainText().toStdString();
            break;

        default:
            throw "Unkown data type for algorithm parameter";
            break;
    }

    return this->algorithmParameter;
}

QWidget*
mseg::AlgorithmParameterWidget::addBoolParameterWidget()
{
    // Init input widget
    QCheckBox* widget = new QCheckBox();

    // Configure input widget
    widget->setChecked((boost::algorithm::to_lower_copy(this->algorithmParameter.value) == "true") ? true : false);
    widget->setToolTip(QString::fromStdString(this->algorithmParameter.description));

    // Return input widget
    return widget;
}

QWidget*
mseg::AlgorithmParameterWidget::addIntParameterWidget()
{
    // Init input widget
    QSpinBox* widget = new QSpinBox();

    // Configure input widget
    widget->setToolTip(QString::fromStdString(this->algorithmParameter.description));
    widget->setMinimum(this->algorithmParameter.intmin);
    widget->setMaximum(this->algorithmParameter.intmax);
    widget->setValue(std::stoi(this->algorithmParameter.value));

    // Return input widget
    return widget;
}

QWidget*
mseg::AlgorithmParameterWidget::addFloatParameterWidget()
{
    // Init input widget
    QDoubleSpinBox* widget = new QDoubleSpinBox();

    // Workaround for a Qt display bug (using system locale, ignoring Locale::setDefault for display)
    widget->setLocale(QLocale());

    // Configure input widget
    widget->setToolTip(QString::fromStdString(this->algorithmParameter.description));
    widget->setDecimals(this->algorithmParameter.decimals);
    widget->setMinimum(this->algorithmParameter.floatmin);
    widget->setMaximum(this->algorithmParameter.floatmax);
    widget->setSingleStep(1.0f / (std::pow(10, this->algorithmParameter.decimals - 1)));
    widget->setValue(this->stringToFloat(this->algorithmParameter.value));

    // Return input widget
    return widget;
}

QWidget*
mseg::AlgorithmParameterWidget::addStringParameterWidget()
{
    // Init input widget
    QLineEdit* widget = new QLineEdit();

    // Configure input widget
    widget->setText(QString::fromStdString(this->algorithmParameter.value));
    widget->setToolTip(QString::fromStdString(this->algorithmParameter.description));

    // Return input widget
    return widget;
}

QWidget*
mseg::AlgorithmParameterWidget::addJsonParameterWidget()
{
    // Init input widget
    QPlainTextEdit* widget = new QPlainTextEdit();

    // Configure input widget
    widget->setPlainText(QString::fromStdString(this->algorithmParameter.value));
    widget->setToolTip(QString::fromStdString(this->algorithmParameter.description));

    // Return input widget
    return widget;
}

double
mseg::AlgorithmParameterWidget::stringToFloat(std::string s)
{
    return QLocale::c().toDouble(QString::fromStdString(s));
}

std::string
mseg::AlgorithmParameterWidget::floatToString(double number)
{
    return QLocale::c().toString(number).replace(",", "").toStdString();
}
