/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::gui-plugins::MSegWidgetController
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_ArmarXCore_MSeg_WidgetController_H
#define _ARMARX_ArmarXCore_MSeg_WidgetController_H

#include <algorithm> // sort
#include <iomanip> // setprecision
#include <sstream> // stringstream
#include <string>
#include <tuple>
#include <vector>

#include <QFileDialog>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QLayoutItem>
#include <QString>
#include <QTimer>
#include <QMessageBox>
#include <QWidget>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <MSeg/interface/DataTypes.h>
#include <MSeg/interface/exceptions.h>
#include <MSeg/interface/Topics.h>
#include <MSeg/interface/WidgetControllerInterface.h>
#include <MSeg/interface/EvaluationControllerInterface.h>
#include <MSeg/interface/SegmentationControllerInterface.h>
#include <MSeg/gui-plugins/MSeg/AlgorithmParameterWidget.h>
#include <MSeg/gui-plugins/MSeg/ui_MSegWidget.h>
#include <MSeg/gui-plugins/MSeg/MotionPlayer.h>

namespace mseg
{
    /**
     * \class MSegWidgetController
     * \brief MSegWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        MSegWidgetController:
    public armarx::ArmarXComponentWidgetController,
    public mseg::WidgetControllerInterface
    {
        Q_OBJECT

    private:

        // Widgets
        Ui::MSegWidget widget;

        // Controller
        mseg::SegmentationControllerInterfacePrx segmentationController;
        mseg::EvaluationControllerInterfacePrx evaluationController;

        // Poll timer
        QTimer* segmentPollTimer;
        QTimer* evaluationPollTimer;

        // Async return ptr
        Ice::AsyncResultPtr segmentAsyncResult;
        Ice::AsyncResultPtr evaluationAsyncResult;

        // Data structures
        mseg::SegmentationResultDataset segmentationResults;

    public:
        /**
         * Controller Constructor
         */
        explicit MSegWidgetController();

        /**
         * Controller destructor
         */
        virtual ~MSegWidgetController() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        virtual void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        virtual void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        virtual QString getWidgetName() const override
        {
            return mseg::MSegWidgetController::GetWidgetName();
        }

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "MSeg";
        }

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override
        {
            return mseg::MSegWidgetController::GetWidgetName().toStdString();
        }

        static QIcon GetWidgetIcon()
        {
            return QIcon(":/images/mseg-icon.svg");
        }

        static QIcon GetWidgetCategoryIcon()
        {
            return mseg::MSegWidgetController::GetWidgetIcon();
        }

        virtual QIcon getWidgetIcon() const override
        {
            return mseg::MSegWidgetController::GetWidgetIcon();
        }

        virtual QIcon getWidgetCategoryIcon() const override
        {
            return mseg::MSegWidgetController::GetWidgetIcon();
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * \see armarx::Component::onConnectComponent()
         */
        virtual void onConnectComponent();

    protected:

        virtual void initConnections();
        virtual void initComboAlgorithms(const std::vector<std::string> = std::vector<std::string>());
        virtual void initPollTimer();

        virtual void updateButtonsSegment();
        virtual void updateButtonEvaluate();

        virtual void displayAlgorithmOfflineException();
        virtual void displayException(std::string, std::string, std::string);

    signals: // Qt signals

        void segmentationProgressChanged(int percent);
        void evaluationProgressChanged(int percent);
        void segmentationStarted();
        void segmentationFinished(bool success, bool autoEvaluate);
        void evaluationStarted();
        void evaluationFinished(bool success);
        void algorithmSelected(std::string algorithmName);

    public slots: // Qt slots

        void onButtonRefreshAlgorithmsClicked();
        void onButtonSegmentCurrentClicked();
        void onButtonSegmentAllClicked();
        void onButtonEvaluateClicked();
        void onComboAlgorithmChanged();

        void onSegmentationProgressChanged(int percent);
        void onEvaluationProgressChanged(int percent);

        void onSegmentPollTimerTimeout();
        void onEvaluationPollTimerTimeout();

        void onDatasetLoading();
        void onDatasetChanged(mseg::MotionRecordingDataset dataset);

        virtual void onAlgorithmSelected(std::string algorithmName);

    public: // MotionViewerListener interface


    public: // SegmentationProgressTopic interface

        void reportSegmentationProgress(int percent, const Ice::Current& c = ::Ice::Current());
        void reportEvaluationProgress(int percent, const Ice::Current& c = ::Ice::Current());

    };
}

#endif
