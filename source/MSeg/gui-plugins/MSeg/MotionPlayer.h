/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::MotionPlayer
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef MOTIONPLAYER_H
#define MOTIONPLAYER_H

#include <string>

#include <boost/shared_ptr.hpp>

#include <QWidget>
#include <QFileDialog>
#include <QtConcurrentRun>

#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/sensors/SoSensor.h>
#include <Inventor/Qt/SoQt.h>

#include <MMM/MMMCore.h>

#include <MMM/Motion/Legacy/LegacyMotionReaderXML.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderC3D.h>
#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <MMM/Model/Model.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMM/Model/ModelProcessorFactory.h>
#include <MMM/Model/ModelProcessor.h>

#include <MMMSimoxTools/MMMSimoxTools.h>

#include <MSeg/components/Utility/Utility.h>
#include <MSeg/gui-plugins/MSeg/Viewer.h>

namespace mseg
{
    enum MotionType {C3D, MMM};

    class MotionPlayer:
        public QObject
    {
        Q_OBJECT

    public:

        MotionPlayer(QWidget* container);
        ~MotionPlayer();
        int main();

        void loadMMM(std::string MMMFile);
        void loadC3D(std::string C3DFile);

        void jumpToFrame(int frame);
        void progress();
        void fpsChanged(double newValue);
        void togglePlay();
        void play();
        void pause();
        MMM::AbstractMotionPtr getCurrentMotion();
        static void timerCB(void* data, SoSensor* sensor);
        void setViewMode(MotionType viewMode);

    signals:

        void playProgress(int frame);
        void fileChanged();

    private:

        Viewer* viewer;
        MotionType viewMode;

        // organizing timer
        SoSensorManager* _pSensorMgr;
        SoTimerSensor* _pSensorTimer;
        int currentFrame;
        int ms_per_frame = 10;

        bool playing;
        MMM::AbstractMotionPtr currentAbstractMotion = nullptr;
        MMM::MarkerMotionPtr currentMarkerMotion = nullptr;
        std::map<std::string, VirtualRobot::RobotPtr>* currentRobots = nullptr;
        std::vector<MMM::LegacyMotionPtr>* currentMotions = nullptr;
        std::map <std::string, VirtualRobot::RobotNodeSetPtr>* currentNodeSets = nullptr;

    };
}

#endif // MOTIONPLAYER_H
