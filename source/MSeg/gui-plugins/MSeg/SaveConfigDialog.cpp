/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::SaveConfigDialog
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SaveConfigDialog.h"

SaveConfigDialog::SaveConfigDialog(QWidget* parent) : QDialog(parent)
{
    this->saveDialog.setupUi(this);

    this->connect(this->saveDialog.comboParameterConfigs, SIGNAL(currentIndexChanged(int)), this, SLOT(onComboSavedConfigsChanged(int)));
    this->connect(this->saveDialog.textName, SIGNAL(textChanged(QString)), this, SLOT(onTextNameChanged(QString)));

    this->saveDialog.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

void
SaveConfigDialog::setComboOptions(std::vector<std::string> options)
{
    this->comboOptions = options;

    this->saveDialog.comboParameterConfigs->clear();
    this->saveDialog.comboParameterConfigs->addItem("New Configuration");

    for (std::string config : options)
    {
        this->saveDialog.comboParameterConfigs->addItem(QString::fromStdString(config));
    }
}

std::string
SaveConfigDialog::getSaveName()
{
    if (this->saveDialog.comboParameterConfigs->currentIndex() == 0)
    {
        return this->saveDialog.textName->text().toStdString();
    }

    return this->saveDialog.comboParameterConfigs->currentText().toStdString();
}

void
SaveConfigDialog::onComboSavedConfigsChanged(int index)
{
    this->saveDialog.textName->setEnabled(index == 0);
    this->onTextNameChanged(this->saveDialog.textName->text());
    if (index != 0)
    {
        this->saveDialog.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
    this->saveDialog.labelError->setText("");
}

void
SaveConfigDialog::onTextNameChanged(QString proposedName)
{
    this->saveDialog.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    this->saveDialog.labelError->setText("");

    for (std::string option : this->comboOptions)
    {
        if (option == proposedName.toStdString())
        {
            this->saveDialog.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
            this->saveDialog.labelError->setText("The name you entered is already in use.");
        }
    }

    if (!QRegExp("^(\\w|\\d|-|_)+$", Qt::CaseInsensitive).exactMatch(proposedName))
    {
        this->saveDialog.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        this->saveDialog.labelError->setText("The configuration name must only contain characters a-z, A-Z, 0-9, - and _  and also must not be empty!");
    }
}
