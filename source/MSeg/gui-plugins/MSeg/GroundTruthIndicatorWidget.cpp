/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::GroundTruthIndicatorWidget
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/gui-plugins/MSeg/GroundTruthIndicatorWidget.h>

mseg::GroundTruthIndicatorWidget::GroundTruthIndicatorWidget(QWidget* parent)
    : QWidget(parent)
{
    this->setMinimumHeight(14);
}

mseg::GroundTruthIndicatorWidget::~GroundTruthIndicatorWidget()
{

}

void
mseg::GroundTruthIndicatorWidget::setMaxFrame(int maxFrame)
{
    this->maxFrame = maxFrame;
}

void
mseg::GroundTruthIndicatorWidget::setKeyFrames(std::vector<std::tuple<int, int> > keyFrames)
{
    this->keyFrames = keyFrames;
}

void
mseg::GroundTruthIndicatorWidget::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);
    painter.setPen(Qt::NoPen);
    painter.setRenderHint(QPainter::Antialiasing);

    // Top and buttom margin (will not be drawn on); in pixel
    int drawMarginBottom = 0;

    // Width for each triangle in pixels (from left to right corner)
    int triangleWidth = 6;

    // Height for each triangle in pixels (from bottom corner to top line of the triangle)
    int triangleHeight = 6;

    // General height and width of the whole draw surface
    int height = this->height();
    int width = this->width();

    // Calculate ratio of widget width and total number of frames
    float pixelFrameRatio = (float) width / (float) this->maxFrame;

    // For each keyframe, draw a triangle
    for (auto keyFrame : this->keyFrames)
    {
        // Declare variables
        int keyFrameNumber;
        int significance;
        QColor color;

        // Unpack tuple into previously declared variables
        std::tie(keyFrameNumber, significance) = keyFrame;

        // Determine color depending on significance
        if (significance == 1)
        {
            color = Qt::yellow;
        }
        else if (significance == 2)
        {
            color = QColor("orange");
        }
        else
        {
            color = Qt::red;
        }

        // Calculate X offset to correlate triangle position indicating
        // keyframe with corresponding timestamp in motion viewer skim
        int offsetX = (int)((float) keyFrameNumber * pixelFrameRatio);

        // Calculate minimum and maximum pixels to determine draw area
        int maxY = height - drawMarginBottom;

        // Calculate bottom corner of the triangle
        int bottomCornerX = offsetX;
        int bottomCornerY = maxY;

        // Calculate top left corner of the triangle (= bottom left corner of color box)
        int topLeftCornerX = offsetX - (triangleWidth / 2);
        int topLeftCornerY = maxY - triangleHeight;

        // Calculate top right corner of the triangle (= bottom right corner of color box)
        int topRightCornerX = offsetX + (triangleWidth / 2);
        int topRightCornerY = maxY - triangleHeight;

        // Calculate top left corner of the color box
        int topLeftColorBoxCornerX = topLeftCornerX;
        int topLeftColorBoxCornerY = topLeftCornerY - 8;

        // Calculate top right corner of the color box
        int topRightColorBoxCornerX = topRightCornerX;
        int topRightColorBoxCornerY = topRightCornerY - 8;

        // Init path to draw triangle
        QPainterPath triangle;

        // Outline triangle
        triangle.moveTo(bottomCornerX, bottomCornerY);
        triangle.lineTo(topLeftCornerX, topLeftCornerY);
        triangle.lineTo(topRightCornerX, topRightCornerY);
        triangle.lineTo(bottomCornerX, bottomCornerY);

        painter.fillPath(triangle, Qt::black);

        // Init path to draw color box
        QPainterPath colorBox;

        // Outline color box
        colorBox.moveTo(topLeftCornerX, topLeftCornerY);
        colorBox.lineTo(topLeftColorBoxCornerX, topLeftColorBoxCornerY);
        colorBox.lineTo(topRightColorBoxCornerX, topRightColorBoxCornerY);
        colorBox.lineTo(topRightCornerX, topRightCornerY);
        colorBox.lineTo(topLeftCornerX, topLeftCornerY);

        painter.fillPath(colorBox, color);
        //painter.drawPath(colorBox);
    }
}
