/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXCore::gui-plugins::MSeg
 * \author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_ArmarXCore_MSeg_GuiPlugin_H
#define _ARMARX_ArmarXCore_MSeg_GuiPlugin_H

#include <QLocale>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>

#include <MSeg/gui-plugins/MSeg/MSegWidgetController.h>

namespace mseg
{
    /**
     * \class MSegGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief MSegGuiPlugin brief description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT MSegGuiPlugin:
        public armarx::ArmarXGuiPlugin
    {
        Q_OBJECT

    public:
        /**
         * All widgets exposed by this plugin are added in the constructor
         * via calls to addWidget()
         */
        MSegGuiPlugin();
    };
}

#endif
