/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::KeyFrameIndicatorWidget
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/gui-plugins/MSeg/KeyFrameIndicatorWidget.h>

mseg::KeyFrameIndicatorWidget::KeyFrameIndicatorWidget(QWidget* parent)
    : QWidget(parent)
{
    this->setMinimumHeight(12);
}

mseg::KeyFrameIndicatorWidget::~KeyFrameIndicatorWidget()
{

}

void
mseg::KeyFrameIndicatorWidget::setMaxFrame(int maxFrame)
{
    this->maxFrame = maxFrame;
}

void
mseg::KeyFrameIndicatorWidget::setKeyFrames(std::vector<int> keyFrames)
{
    this->keyFrames = keyFrames;
}

void
mseg::KeyFrameIndicatorWidget::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);
    painter.setPen(Qt::NoPen);
    painter.setRenderHint(QPainter::Antialiasing);

    // Top and buttom margin (will not be drawn on); in pixel
    int drawMarginTop = 0;

    // Width for each triangle in pixels (from left to right corner)
    int triangleWidth = 6;

    // Height for each triangle in pixels (from top corner to bottom line)
    int triangleHeight = 6;

    // General width of the whole draw surface
    int width = this->width();

    // Calculate ratio of widget width and total number of frames
    float pixelFrameRatio = (float) width / (float) this->maxFrame;

    // For each keyframe, draw a triangle
    for (int keyFrame : this->keyFrames)
    {
        // Calculate X offset to correlate triangle position indicating
        // keyframe with corresponding timestamp in motion viewer skim
        int offsetX = (int)((float) keyFrame * pixelFrameRatio);

        // Calculate minimum and maximum pixels to determine draw area
        int minY = drawMarginTop;

        // Calculate top corner of the triangle
        int topCornerX = offsetX;
        int topCornerY = minY;

        // Calculate bottom left corner of the triangle
        int bottomLeftCornerX = offsetX - (triangleWidth / 2);
        int bottomLeftCornerY = minY + triangleHeight;

        // Calculate bottom right corner of the triangle
        int bottomRightCornerX = offsetX + (triangleWidth / 2);
        int bottomRightCornerY = minY + triangleHeight;

        // Init path to draw triangle
        QPainterPath triangle;

        // Outline triangle
        triangle.moveTo(topCornerX, topCornerY);
        triangle.lineTo(bottomLeftCornerX, bottomLeftCornerY);
        triangle.lineTo(bottomRightCornerX, bottomRightCornerY);
        triangle.lineTo(topCornerX, topCornerY);

        painter.fillPath(triangle, Qt::black);
    }
}
