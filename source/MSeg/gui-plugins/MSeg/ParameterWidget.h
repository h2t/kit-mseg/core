/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::ParameterWidget
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef PARAMETERCONTROLLER_H
#define PARAMETERCONTROLLER_H

#include <algorithm>
#include <vector>
#include <string>

#include <boost/algorithm/string.hpp>

#include <jsoncpp/json/json.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>

#include <QWidget>
#include <QFormLayout>
#include <QLabel>
#include <QString>
#include <QRegExp>
#include <QInputDialog>
#include <QMessageBox>

#include <ui_ParameterWidget.h>
#include <MSeg/interface/DataTypes.h>
#include <MSeg/gui-plugins/MSeg/AlgorithmParameterWidget.h>
#include <MSeg/gui-plugins/MSeg/SaveConfigDialog.h>
#include <MSeg/components/Utility/Utility.h>

namespace mseg
{
    class ParameterWidget:
        public QWidget
    {
        Q_OBJECT
    public:
        explicit ParameterWidget(QWidget* parent);
        virtual ~ParameterWidget();

        void reset();
        void clearParameterWidgets();
        std::vector<mseg::AlgorithmParameter> getParameterConfig();
        void setParameterConfig(std::vector<mseg::AlgorithmParameter> algorithmParameters);
        void setSelectedAlgorithm(std::string algorithmName, std::vector<mseg::AlgorithmParameter> defaultConfig);
        void deselectAlgorithm();
        std::vector<std::string> getSavedConfigs();
        std::vector<mseg::AlgorithmParameter> loadParameterConfig(std::string name);

    public slots:
        void onComboParameterConfigsChanged(int index);
        void onButtonSaveClicked();
        void onButtonDeleteClicked();

    protected:
        void updateComboParameterConfigOptions();
        void displayException(std::string title, std::string message);
        int findParameterIndexInDefaults(std::string parameterName);
        std::string configToJson(std::vector<mseg::AlgorithmParameter> config);

    private:

        // Widgets
        Ui::ParameterWidget widget;
        QWidget* widgetParameter;
        std::string selectedAlgorithm;
        std::vector<mseg::AlgorithmParameter> selectedDefaults;
        std::string selectedConfigJson;
    };
}

#endif // PARAMETERCONTROLLER_H
