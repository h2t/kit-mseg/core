/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::gui-plugins::MotionSetPlayerWidget
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_ArmarXCore_MSeg_MotionSetWidgetController_H
#define _ARMARX_ArmarXCore_MSeg_MotionSetWidgetController_H

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <MSeg/components/Utility/Utility.h>
#include <MSeg/interface/DataTypes.h>
#include <MSeg/gui-plugins/MSeg/MotionPlayer.h>
#include <MSeg/gui-plugins/MSeg/ui_MotionSetPlayerWidget.h>

#include <QtConcurrentRun>

namespace mseg
{
    /**
     * \class MSegWidgetController
     * \brief MSegWidgetController brief one line description
     *
     * Detailed description
     */
    class
        MotionSetPlayerWidget:
    public QWidget
    {
        Q_OBJECT

    public:

        explicit MotionSetPlayerWidget(QWidget* parent);
        virtual ~MotionSetPlayerWidget();

        void playNext();
        void playPrevious();
        void playFirst();
        void playLast();

        void setCurrent(int index);
        int getCurrent();
        QString getCurrentFilename();
        bool isMotionSetOpen();

        virtual void setSegmentationResults(std::vector<mseg::SegmentationResult> segmentationResults);
        virtual void updateSegmentationResult();
        virtual void clearSegmentationResults();
        virtual void updateComboDataset();

    protected:


    private:

        // Widgets
        Ui::MotionSetPlayerWidget widget;

        mseg::MotionPlayer* player;
        bool playing = false;

        mseg::MotionRecordingDataset dataset;
        std::vector<mseg::SegmentationResult> segmentationResults;

        bool hasDatasetChanged = false;

    signals: // Qt signals

        void datasetLoading();
        void datasetChanged(mseg::MotionRecordingDataset);

    public slots: // Qt slots

        void onButtonOpenDatasetClicked();
        void onButtonTogglePlayClicked();
        void onButtonStopClicked();
        void onButtonRewindClicked();
        void onButtonForwardClicked();
        void onButtonPrevMotionClicked();
        void onButtonNextMotionClicked();
        void onComboDatasetChanged(int index);
        void onSliderMotionChanged(int val);
        void onSegmentationStarted();
        void onSegmentationFinished(bool success, bool autoEvaluate);
        void onEvaluationStarted();
        void onEvaluationFinished(bool success);

        void onPlayProgress(int frame);
        void onFileChanged();

    };
}

#endif
