## Staging

*No changes yet*


## Version 1.2

**Bugfixes**

- Fix a CMake dependency problem
- Enable evaluation result export button only if results arrive (#83)

**Features**

- Support proper packaging and supply binaries (#82)
- Add MSeg icon to GUI (#84)


## Version 1.1

**Features**

- Implement parameter save/load (#4)
- Allow exporting evaluation results as XML (#70)

**Bugfixes**

- Load motions in the background (#65)
- Disable segment current button (not yet implemented) (#67)
- Fix a bug where motions are loaded twice (#68)
- Disable segment and evaluate buttons while a new dataset ist being opened (#71)
- Fix an issue where thousands separators could cause crashes (#75)
- Fix issues with a more recent version of MMMTools (#80)


## Version 1.0

**Bugfixes**

- Fix an unimplemented method preventing Java PLI from compiling (#31)
- Minor fixes on the UI (#30)
- Fix MATLAB dependency issues (#56)
- Fix stacktrace output for MATLAB exceptions (#59)
- Made everything locale independent (#33, #61)
- Fix various UI issues (#44, #60, #64)
- Fix a bug where shutting down a Java algorithm would raise an exception

**Features**

- Include ZVC algorithm (#25)
- Allow algorithms to require training data of a certain granularity (#27)
- Add `mseg` tool
- Add subcommand to `mseg` to generate a C++ skeleton project (#35)
- Add subcommand to `mseg` to generate a Python skeleton project (#36)
- Add commands to `mseg` to start/stop/restart the core module (#37)
- Add subcommand to `mseg` to generate a Java skeleton project (#40)
- Include MATLAB PLI (#23)
- Add subcommand to `mseg` to generate a MATLAB skeleton project (#42) 
- Improved error handling (#47, #49, #50)
- Refactor algorithm name handling (#48)
- Unify PLI output (#45)
